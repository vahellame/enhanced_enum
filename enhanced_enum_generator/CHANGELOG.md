## 0.1.3
- chore: update dependencies (Lanna Michalke)

## 0.1.2
- Allow setting prefixes in EnhancedEnum and generating names for each of them

## 0.1.1
- Do the calculation of enum value -> string on compile-time
- Introduce strict enums
- Better name for the fromString method (`MyEnum.values.fromString`)

## 0.1.0
- Initial version.
